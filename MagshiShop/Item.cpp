#include "Item.h"

Item::Item()
{
}

Item::Item(string, string, double)
{
}

Item::~Item()
{
}

void Item::setCount(int count)
{
	this->_count = count;
}

void Item::setName(string name)
{
	this->_name = name;
}

void Item::setSerialNumber(string serial)
{
	this->_serialNumber = serial;
}

void Item::setUnitPrice(double unitPrice)
{
	this->_unitPrice = unitPrice;
}

int Item::getCount()
{
	return this->_count;
}

double Item::getUnitPrice()
{
	return this->_unitPrice;
}

string Item::getName()
{
	return this->_name;
}

string Item::getSerialNumber()
{
	return this->_serialNumber;
}

double Item::totalPrice() const
{
	return this->_unitPrice * this->_count;
}

bool Item::operator<(const Item & other) const
{
	if (this->_serialNumber.compare(other._serialNumber) < 0)
		return true;
	else
		return false;
}

bool Item::operator>(const Item & other) const
{
	return !this->operator<(other);
}

bool Item::operator==(const Item & other) const
{
	return !this->operator<(other) && !this->operator>(other);
}
