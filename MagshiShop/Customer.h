#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item itemToAdd);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions
	string getName();
	set<Item> getItems();
	void setName(string s);
	void setItems(set<Item> items);
private:
	string _name;
	set<Item> _items;


};
